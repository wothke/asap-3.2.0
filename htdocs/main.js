let songs = [
		'Slight Atari Player/Sack/smells like teen spirit.sap;0;60',
		'Slight Atari Player/Kjmann/tempest xtreem song one.sap;0;60',
		'Slight Atari Player/- unknown/archon.sap;0;45',
		'Slight Atari Player/- unknown/ballblazer.sap;0;45',
		'Slight Atari Player/Poison/blade runner.sap;0;60',
		'Slight Atari Player/Peter Liepa/boulder dash.sap;0;45',
		'Slight Atari Player/Rob Hubbard/extirpator.sap;0;60',
		'Slight Atari Player/Miker/ghosts n goblins.sap;0;60',
		'Slight Atari Player/Marucha/lemmings prevision.sap;0;60',
		'Slight Atari Player/Torsten Karwoth/megablast.sap;0;60',
		'Slight Atari Player/Sack/monty on the run game over.sap;0;240',
		'Slight Atari Player/ZBX Virus/pentagram 4 axel f.sap;0;60',
		'Slight Atari Player/Rob Hubbard/warhawk.sap;0;45',
		'Slight Atari Player/Miker/yie ar kung fu title.sap;0;120',
	];

class ASAPDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "Atari 8-bit";}
	getDisplaySubtitle() 	{ return "music nostalgia";}
	getDisplayLine1() { return this.getSongInfo().songName;}
	getDisplayLine2() { return this.getSongInfo().songAuthor; }
	getDisplayLine3() { return this.getSongInfo().songReleased; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new ASAPBackendAdapter();
//		this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let arr = someSong.split(";");
					let track = arr.length > 1 ? parseInt(arr[1]) : -1;
					let timeout = arr.length > 2 ? parseInt(arr[2]) : -1;
					let name = arr[0];

					// drag&dropped temp files start with "/tmp/"
					name = name.startsWith("/tmp/") ? name : window.location.protocol + "//ftp.modland.com/pub/modules/" + name;

					let options = {};
					options.track = track;
					options.timeout = timeout;

					return [name, options];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new ASAPDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x4a3669, 0x4a3e70, 0x4a7763, 0x699522], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}