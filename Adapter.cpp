/*
* This is the interface exposed by Emscripten to the JavaScript world..
*
* Copyright (C) 2015-2023 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <emscripten.h>

#include "asap.h"
extern "C" void set_boost_volume(unsigned char b);

#include "MetaInfoHelper.h"

using emsutil::MetaInfoHelper;



#define MAX_SCOPE_BUFFERS 2								// left/right

namespace asap {
	class Adapter {
	public:
		Adapter() : _sampleRate(44100), _channels(0), _sampleBufferLen(0), _sampleBufferAvailable(0), _sampleBuffer(NULL),
					_asap(NULL), _info(NULL), _song_duration_ms(0), _scopesEnabled(false)
		{
		}

		int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: ASAP's sampleRate is currently hardcoded

			allocBuffer(audioBufSize);

			if (_asap == NULL)
			{
				_asap = ASAP_New();
			}

			_scopesEnabled = scopesEnabled;

			if (_scopesEnabled)
			{					// for some reason allocating this dynamically introduced noise/clicks..
				if (_scopeBuffers[0] == 0)
				{
					for (int i= 0; i<MAX_SCOPE_BUFFERS; i++) {
						_scopeBuffers[i]= (int16_t*) calloc(sizeof(int16_t), 16384); 	// just alloc enough for the possible maximum
					}
				}
			}

			bool r = ASAP_Load(_asap, filename, (unsigned char*) inBuffer, inBufSize);

			if (r)
			{
				// tracks do not have separate meta info
				_info = ASAP_GetInfo(_asap);

				updateSongInfo();

				_channels = ASAPInfo_GetChannels(_info);
			}
			return !r;
		}

		int setSubsong(int track)
		{
			_song_duration_ms = ASAPInfo_GetDuration(_info, track);

			bool r = ASAP_PlaySong(_asap, track, _song_duration_ms);
			return !r;
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();

			if (_asap != NULL) {
				ASAP_Delete(_asap);
				_asap = NULL;
			}
		}

		void setBoost(int boost)
		{
			set_boost_volume(boost & 0xff);
		}

		int getSampleRate()
		{
			return _sampleRate;
		}

		int getMaxPosition()
		{
			return _song_duration_ms;
		}

		int getCurrentPosition()
		{
			return ((long long)ASAP_GetBlocksPlayed(_asap)) * 1000 / ASAP_SAMPLE_RATE;
		}

		void seekPosition(int ms)
		{
			ASAP_Seek(_asap, ms);
		}

		// fixme: one POKEY chip supposedly has 4 voices/channels each of which can
		// output a configurable rectangle-wave, voices can then be pair-wise combined..
		// via CPU digi-samples can be played.. i.e. respectice voice streams could be
		// shown to provide more detailed output (currently only L/R is implemented)

		int getNumberTraceStreams()
		{
			return _scopesEnabled ? _channels : 0;
		}

		const char** getTraceStreams()
		{
			return (const char**)_scopeBuffers;
		}

		int genSamples()
		{
			// fixme: ASAPInfo_GetLoop(_info, song) not used..

			int numBytesToFill = _sampleBufferLen * _channels * sizeof(int16_t);
			int len = ASAP_Generate(_asap, (unsigned char*)_sampleBuffer, numBytesToFill, ASAPSampleFormat_S16_L_E, _scopesEnabled ? (unsigned char**) _scopeBuffers : 0);
			len = (len < 0 ) ? 0 : len >> 1;

			if (_channels == 1)
			{
				// faster to do this here on the WASM side

				int dstIdx = (len<<1) - 1;
				for (int i = len-1; i >= 0; i--) {
					_sampleBuffer[dstIdx--] = _sampleBuffer[i];
					_sampleBuffer[dstIdx--] = _sampleBuffer[i];
				}
			}
			_sampleBufferAvailable = len / _channels;

			return (_sampleBufferAvailable <= 0 ) ? 1 : 0;
		}

		char* getSampleBuffer()
		{
			return (char*)_sampleBuffer;
		}

		int getSampleBufferLength()
		{
			return _sampleBufferAvailable;
		}

		int getNumberChannels()
		{
			return 2; // _channels;         see genSamples() - always generating stereo
		}
	private:
		void allocBuffer(int len)
		{
			if (len > _sampleBufferLen)
			{
				if (_sampleBuffer) free(_sampleBuffer);
				_sampleBuffer = (int16_t*)malloc(sizeof(int16_t) * len * 2);
				_sampleBufferLen = len;
			}
		}

		void updateSongInfo()
		{
			MetaInfoHelper *info = MetaInfoHelper::getInstance();
			info->setText(0, ASAPInfo_GetTitleOrFilename(_info), "");
			info->setText(1, ASAPInfo_GetAuthor(_info), "");
			info->setText(2, ASAPInfo_GetDate(_info), "");

			int n = ASAPInfo_GetSongs(_info);
			n = (n > 1) ? n : 1;
			snprintf(_songMaxNoTxt, 3, "%d", n);
			info->setText(3, _songMaxNoTxt, "");

			int d = ASAPInfo_GetDefaultSong(_info);
			snprintf(_songNoTxt, 3, "%d", d);
			info->setText(4, _songNoTxt, "");
		}
	private:
		int _sampleRate;
		int _channels;

		char _songMaxNoTxt[4];
		char _songNoTxt[4];

		int _sampleBufferLen;
		int _sampleBufferAvailable;
		int16_t* _sampleBuffer;

		ASAP* _asap;
		const ASAPInfo* _info;
		int _song_duration_ms;

		bool _scopesEnabled;
		int16_t* _scopeBuffers[MAX_SCOPE_BUFFERS];
	};
};

asap::Adapter _adapter;



// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard functions
EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return _adapter.setSubsong(track);  }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))				{ _adapter.seekPosition(ms);  }
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
EMBIND(int, emu_number_trace_streams())				{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())		{ return _adapter.getTraceStreams(); }


// --- add-on
EMBIND(void, emu_set_boost(int boost))				{ _adapter.setBoost(boost); }
EMBIND(int, emu_get_number_channels())				{ return _adapter.getNumberChannels(); }

