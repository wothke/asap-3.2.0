/*
 asap_adapter.js: Adapts ASAP backend to generic WebAudio/ScriptProcessor player.

   version 1.1
   copyright (C) 2015-2023 Juergen Wothke

 LICENSE

 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version. This library is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/
class ASAPBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(scopeEnabled)
	{
		super(backend_ASAP.Module, 2, new SimpleFileMapper(backend_ASAP.Module),
				new HEAP16ScopeProvider(backend_ASAP.Module, 0x8000));

		this._scopeEnabled = (typeof scopeEnabled == 'undefined') ? false : scopeEnabled;

		this.ensureReadyNotification();
	}

	enableScope(enable)
	{
		this._scopeEnabled = enable;
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), 1024, this._scopeEnabled);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);

			this._channels = this.Module.ccall('emu_get_number_channels', 'number');
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let boostVolume= (options.boostVolume) ? options.boostVolume : 0;
		this.Module.ccall('emu_set_boost', 'number', ['number'], [boostVolume]);

		this.updateSongInfo("");
		let id = (options.track < 0) ? this._songInfo.actualSubsong : options.track;

		let r = this.Module.ccall('emu_set_subsong', 'number', ['number'], [id]);

		return r;
	}

	getSongInfoMeta()
	{
		return {
			songName: String,
			songAuthor: String,
			songReleased: String,
			maxSubsong: Number,
			actualSubsong: Number
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 5;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);
		result.songName = this.Module.Pointer_stringify(array[0]);
		if (!result.songName.length) result.songName = this._makeTitleFromPath(filename);

		result.songAuthor = this.Module.Pointer_stringify(array[1]);
		result.songReleased = this.Module.Pointer_stringify(array[2]);
		result.maxSubsong = parseInt(this.Module.Pointer_stringify(array[3]));
		result.actualSubsong = parseInt(this.Module.Pointer_stringify(array[4]));
	}
};
